#!/usr/bin/python3

import sys
import math

P = {
	"PRECISION": 6, # precision of the sphere approximation
	"ELEVATION_MAX": 10.0, # mountain elevation
	"PLANET_SCALE": 100.0, # sphere size

	# Sun movement parameters.
	"DAY_LENGTH": 24,
	"YEAR_LENGTH": 96,
	"TILT": math.tau * (23 / 360),

	"SEA_LEVEL": 0.5, # between 0 and 1
	"DESERT_LEVEL": 0.2, # between 0 and 1
	"ICE_LEVEL": 0.5, # between 0 and 1

	"WINDOW_WIDTH": 1024,
	"WINDOW_HEIGHT": 1024,

	# WIDTH == 2 * HEIGHT /!\ important for pole & biome marbeling
	"IMAGE": None,
	"WIDTH": 512,
	"HEIGHT": 256,
	"STATIC": False,

	#terrain noise params
	"OCTAVES": 4,
	"PERSISTENCE": 0.5,
	"ISLANDS": 4 #the smaller it is, the more we zoom on the noise field
}

parameters_help = """arguments:
	--help          print this message
	--precision     precision of the sphere approximation (default: 6)
	--elevation     elevation of mountains (default: 10.0)
	--planet_size   size of the planet (default: 100.0)
	--day_length    length of a day in seconds (default: 24)
	--year_length   length of a year in seconds (default: 96)
	--planet_tilt   tilt of planet axis in degrees (default: 23)
	--seas          proportion of seas on the surface (default: 0.5)
	--deserts       proportion of deserts (default: 0.2)
	--ice           proportion of ice at the poles (default: 0.5)
	--texture       texture file to use (by default a new texture is generated)
	--static        load a static texture without elevation (default: false)
	--width         width of generated texture (width == 2 * height) (default: 512)
	--height        height of generated texture (width == 2 * height) (default: 256)
	--octaves       number of octaves, more octaves = more details (deflaut: 4)
	--persistence   persistence of the details in terrain generation (default:0.5)
	--islands       smaller = bigger islands (default: 4)
"""

def parameters_init():
	global P

	for arg in sys.argv[1:]:
		pair = arg.split("=")
		if (len(pair) != 2) or (not pair[0].startswith("--")):
			print("invalid argument {}".format(arg))
			print(parameters_help)
			exit()
		pair[0] = pair[0][2:]

		try:
			if pair[0] == "precision":
				P["PRECISION"] = int(pair[1])
			elif pair[0] == "elevation":
				P["ELEVATION_MAX"] = float(pair[1])
			elif pair[0] == "planet_size":
				P["PLANET_SCALE"] = float(pair[1])
			elif pair[0] == "day_length":
				P["DAY_LENGTH"] = int(pair[1])
			elif pair[0] == "year_length":
				P["YEAR_LENGTH"] = int(pair[1])
			elif pair[0] == "planet_tilt":
				P["TILT"] = math.tau * (float(pair[1]) / 360)
			elif pair[0] == "seas":
				P["SEA_LEVEL"] = float(pair[1])
			elif pair[0] == "deserts":
				P["DESERT_LEVEL"] = float(pair[1])
			elif pair[0] == "ice":
				P["ICE_LEVEL"] = 1.0 - float(pair[1])
			elif (pair[0] == "image") or (pair[0] == "texture"):
				P["IMAGE"] = pair[1]
			elif pair[0] == "width":
				P["WIDTH"] = int(pair[1])
			elif pair[0] == "height":
				P["HEIGHT"] = int(pair[1])
			elif pair[0] == "static":
				P["STATIC"] = bool(pair[1])
			elif (pair[0] == "octaves"):
				P["OCTAVES"] = int(pair[1])
			elif (pair[0] == "persistence"):
				P["PERSISTENCE"] = float(pair[1])
			elif (pair[0] == "islands"):
				P["ISLANDS"] = int(pair[1])
			else:
				print(parameters_help)
				exit()
		except:
			print(parameters_help)
			exit()
