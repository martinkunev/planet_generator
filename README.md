# Planet Generator

Generates the surface of a planet using perlin noise to get oceans, mountains, ice caps and deserts. Visalizes the planet as a mesh in 3D.

## Implementation

Quaternery triangular mesh (https://www.youtube.com/watch?v=SY0bJSF_F5M) - start with an octahedron. Divide each face in 2.

Equirectangular projection for texture, using 2D planar coordinates.
	mapping: coordinates -> triangle in the sphere

normals match vertices
