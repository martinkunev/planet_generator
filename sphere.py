#!/usr/bin/python3

import math
import numpy
import random
import os

from PIL import Image

import perlin
from parameters import P

EPSILON = 1e-6

X = 0
Y = 1
Z = 2

def faces(iterations):
	def middle_project(a, b):
		"""Projects the middle of an edge on the surface of the sphere"""

		x = (a[0] + b[0]) / 2
		y = (a[1] + b[1]) / 2
		z = (a[2] + b[2]) / 2
		norm = math.sqrt(x * x + y * y + z * z) #+ perlin.noise(x,y,z)*.001
		return (x / norm, y / norm, z / norm)

	# Start with an octagon.
	faces = {
		# Face is enclosed in counterclockwise direction.
		((0.0, 1.0, 0.0), (0.0, 0.0, 1.0), (1.0, 0.0, 0.0)),
		((0.0, 1.0, 0.0), (1.0, 0.0, 0.0), (0.0, 0.0, -1.0)),
		((0.0, 1.0, 0.0), (0.0, 0.0, -1.0), (-1.0, 0.0, 0.0)),
		((0.0, 1.0, 0.0), (-1.0, 0.0, 0.0), (0.0, 0.0, 1.0)),
		((0.0, -1.0, 0.0), (1.0, 0.0, 0.0), (0.0, 0.0, 1.0)),
		((0.0, -1.0, 0.0), (0.0, 0.0, 1.0), (-1.0, 0.0, 0.0)),
		((0.0, -1.0, 0.0), (-1.0, 0.0, 0.0), (0.0, 0.0, -1.0)),
		((0.0, -1.0, 0.0), (0.0, 0.0, -1.0), (1.0, 0.0, 0.0)),
	}

	# Divide each equilateral triangle into 4 equilateral triangles.
	for i in range(iterations):
		new = set()
		for face in faces:
			middle_01 = middle_project(face[0], face[1])
			middle_12 = middle_project(face[1], face[2])
			middle_20 = middle_project(face[2], face[0])

			new.add((middle_01, face[1], middle_12))
			new.add((middle_12, face[2], middle_20))
			new.add((middle_20, face[0], middle_01))
			new.add((middle_01, middle_12, middle_20))
		faces = new

	return faces

def coordinates(face):
	"""Calculates the texture coordinates for the vertices of a single triangle"""

	def coordinates_calculate(vector):
		def bounded(number, low=-math.inf, high=math.inf):
			number = max(number, low)
			number = min(number, high)
			return number

		# calculate angle cosines
		cos_horizontal = 0
		cos_vertical = math.sqrt(bounded(1 - vector[Y] * vector[Y], 0))
		if cos_vertical:
			cos_horizontal = vector[X] / cos_vertical

		# calculate angles
		longitude = math.acos(bounded(cos_horizontal, -1, 1))
		if vector[Z] > 0:
			longitude = math.tau - longitude
		latitude = math.acos(bounded(cos_vertical, -1, 1))
		if vector[Y] > 0:
			latitude *= -1

		# normalize angles
		if (abs(longitude) < EPSILON) and zero_max:
			longitude = 1.0
		else:
			longitude = longitude / math.tau
		latitude = latitude / (math.tau / 2) + 0.5

		return longitude, latitude

	def normalize(vector):
		norm = math.sqrt(vector[X] * vector[X] + vector[Y] * vector[Y] + vector[Z] * vector[Z])
		return (vector[X] / norm, vector[Y] / norm, vector[Z] / norm)

	face = tuple(map(normalize, face))

	# By construction of the octagon, z coordinate in any given face always has the same sign.
	# Some vertex may have z == 0 so we need a hint about face orientation.
	assert (face[0][Z] >= 0 and face[1][Z] >= 0 and face[2][Z] >= 0) or (face[0][Z] <= 0 and face[1][Z] <= 0 and face[2][Z] <= 0)
	zero_max = (face[0][Z] + face[1][Z] + face[2][Z] > 0)

	# The x coordinate of a vertex at the pole is ambiguous.
	# By construction of the octagon, at most one vertex of each face is at the pole.
	# We can calculate its x coordinate as a mean of those of the other two vectors.
	# Order vertices from equator to poles so that the vertex at the pole is last.
	indices = sorted((0, 1, 2), key=lambda index: abs(face[index][Y]))

	result = [None] * 3
	for index in indices:
		result[index] = coordinates_calculate(face[index])

	# Fix x coordinate at the pole.
	if abs(face[indices[2]][Y]) == 1:
		result[indices[2]] = ((result[indices[0]][0] + result[indices[1]][0]) / 2, result[indices[2]][1])

	return result[0] + result[1] + result[2]

def surface(filename, static=False):
	"""If a filename is provided, loads the surface of a planet from an image file.
	Else, generates the surface of a planet using Perlin noise and saves it into a file."""

	if static:
		image = Image.open(filename)
		width, height = (image.size[0], image.size[1])
		imageData = image.convert("RGBA").tobytes()
		image.close()
		return imageData, width, height, numpy.zeros((height, width))
	elif filename:
		image = Image.open(filename)
		width, height = (image.size[0], image.size[1])
		imageData = numpy.array(image, dtype=numpy.float) / 255
		image.close()

		img = Image.open("ice.png")
		iceData = numpy.array(img, dtype=numpy.float) / 255
		img = Image.open("desert.png")
		desertData = numpy.array(img, dtype=numpy.float) / 255

		return perlin.texture_load(imageData, iceData, desertData, width, height), width, height, imageData
		#return perlin.texture(imageData, width, height), width, height, imageData

	noise = perlin.landmass(P["WIDTH"], P["HEIGHT"])
	perlin_texture = perlin.texture(noise, P["WIDTH"], P["HEIGHT"])
	i = 0
	while os.path.exists("noise_{}_1landmass.png".format(i)):
		i += 1
	Image.fromarray(numpy.array(noise * 255, dtype="uint8"), "L").save("noise_{}_1landmass.png".format(i))
	Image.fromarray(perlin_texture[1], "L").save("noise_{}_2ice.png".format(i))
	Image.fromarray(perlin_texture[2], "L").save("noise_{}_3desert.png".format(i))
	Image.fromarray(perlin_texture[3], "L").save("noise_{}_4desert.png".format(i))
	Image.fromarray(perlin_texture[4], "L").save("noise_{}_5desert.png".format(i))
	Image.fromarray(perlin_texture[0]).save("noise_{}_5final_texture.png".format(i))
	return perlin_texture[0], P["WIDTH"], P["HEIGHT"], noise
