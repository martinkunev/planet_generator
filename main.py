#!/usr/bin/python3

import sys
import math
import numpy
import ctypes
import time

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

import sphere
from parameters import *

# TODO fix perspective handling for rectangular screen
# TODO when zooming in too much, the camera enters inside the sphere and sees the other side
# TODO implement globe dragging
# TODO clouds
# TODO turning earth
# TODO pre-generate high quality texture
# TODO desert depending on tilt

angle_horizontal = 0.0
angle_vertical = 0.0
look_slope = 0.0
distance = 200

X = 0
Y = 1
Z = 2

drag_x = None
drag_y = None

TEXTURE_INPUT = None

def keyboard(*args):
	global angle_horizontal, angle_vertical, look_slope
	if args[0] == b'\x1b':
		sys.exit()
	elif args[0] == GLUT_KEY_PAGE_UP:
		look_slope += 0.02
	elif args[0] == GLUT_KEY_PAGE_DOWN:
		look_slope -= 0.02
	elif args[0] == GLUT_KEY_LEFT:
		if math.cos(angle_vertical) >= 0:
			angle_horizontal -= 0.02
		else:
			angle_horizontal += 0.02
	elif args[0] == GLUT_KEY_RIGHT:
		if math.cos(angle_vertical) >= 0:
			angle_horizontal += 0.02
		else:
			angle_horizontal -= 0.02
	elif args[0] == GLUT_KEY_UP:
		angle_vertical += 0.02
	elif args[0] == GLUT_KEY_DOWN:
		angle_vertical -= 0.02

def mouse(button, state, x, y):
	global distance
	if state == GLUT_DOWN:
		if button == 1:
			pass
			#drag_x, drag_y = x, y
	elif state == GLUT_UP:
		if button == 1:
			pass
			#if abs(x - drag_x) > DRAG_THRESHOLD:
		elif button == 3:
			distance -= 0.01 * distance
			distance = max(distance, 100)
		elif button == 4:
			distance += 0.01 * distance
			distance = min(distance, 500)

"""def normal(face):
	a = (face[1][X] - face[0][X], face[1][Y] - face[0][Y], face[1][Z] - face[0][Z])
	b = (face[2][X] - face[1][X], face[2][Y] - face[1][Y], face[2][Z] - face[1][Z])

	# calculate normal as a cross product
	x = a[1] * b[2] - a[2] * b[1]
	y = a[2] * b[0] - a[0] * b[2]
	z = a[0] * b[1] - a[1] * b[0]
	return (x, y, z)"""

def globe_init(altitude):
	"""Initializes OpenGL data for displaying the globe"""

	def position(vertex, x, y):
		x = 0 if (x == 1) else int(x * altitude.shape[1])
		y = 0 if (y == 1) else int(y * altitude.shape[0])

		elevation = P["ELEVATION_MAX"] * max(0, altitude[y, x] - P["SEA_LEVEL"]) / (1 - P["SEA_LEVEL"])
		vertex = numpy.array(vertex) * (P["PLANET_SCALE"] + elevation)
		return float(vertex[0]), float(vertex[1]), float(vertex[2])

	faces = sphere.faces(P["PRECISION"])
	vertices = []
	texture = []

	for face in faces:
		coordinates = sphere.coordinates(face)
		vertices += position(face[0], *coordinates[:2])
		vertices += position(face[1], *coordinates[2:4])
		vertices += position(face[2], *coordinates[4:])
		texture += coordinates

	data_vertices = numpy.array(vertices, dtype="float32")
	data_texture = numpy.array(texture, dtype="float32")

	glEnableClientState(GL_VERTEX_ARRAY)
	glEnableClientState(GL_NORMAL_ARRAY)
	glEnableClientState(GL_TEXTURE_COORD_ARRAY)

	buffer_vertices, buffer_textures = glGenBuffers(2)
	glBindBuffer(GL_ARRAY_BUFFER, buffer_vertices)
	glBufferData(GL_ARRAY_BUFFER, len(data_vertices) * ctypes.sizeof(ctypes.c_float), (ctypes.c_float * len(data_vertices))(*data_vertices), GL_STATIC_DRAW)
	glBindBuffer(GL_ARRAY_BUFFER, buffer_textures)
	glBufferData(GL_ARRAY_BUFFER, len(data_texture) * ctypes.sizeof(ctypes.c_float), (ctypes.c_float * len(data_texture))(*data_texture), GL_STATIC_DRAW)

	return buffer_vertices, buffer_textures, len(data_vertices) // 3

def display_square():
	glColor3f(1.0, 1.0, 1.0)
	glBindTexture(GL_TEXTURE_2D, texture)

	glEnable(GL_TEXTURE_2D)

	glBegin(GL_QUADS)
	glTexCoord2f(0, 0); glVertex3f(-50, -50, 50)
	glTexCoord2f(1, 0); glVertex3f(50, -50, 50)
	glTexCoord2f(1, 1); glVertex3f(50, 50, 50)
	glTexCoord2f(0, 1); glVertex3f(-50, 50, 50)
	glEnd()

	glDisable(GL_TEXTURE_2D)

def display_sphere():
	glColor3f(1.0, 1.0, 1.0)
	glBindTexture(GL_TEXTURE_2D, texture)
	
	glBindBuffer(GL_ARRAY_BUFFER, buffer_vertices)
	glVertexPointer(3, GL_FLOAT, ctypes.sizeof(ctypes.c_float) * 3, None)
	glNormalPointer(GL_FLOAT, ctypes.sizeof(ctypes.c_float) * 3, None) # sphere center is at (0, 0) so the normal is the same vector as the point

	glBindBuffer(GL_ARRAY_BUFFER, buffer_textures)
	glTexCoordPointer(2, GL_FLOAT, ctypes.sizeof(ctypes.c_float) * 2, None)

	glDrawArrays(GL_TRIANGLES, 0, vertices_count)

def display():
	glEnable(GL_DEPTH_TEST)
	glEnable(GL_NORMALIZE)
	glEnable(GL_LIGHTING)
	glEnable(GL_LIGHT0)

	glEnable(GL_TEXTURE_2D)

	glLightfv(GL_LIGHT0, GL_AMBIENT, (0.8, 0.8, 0.8, 1.0))
	glLightfv(GL_LIGHT0, GL_DIFFUSE, (1.0, 1.0, 1.0, 1.0))
	glLightfv(GL_LIGHT0, GL_SPECULAR, (0.0, 0.0, 0.0, 1.0))
	glLightfv(GL_LIGHT0, GL_POSITION, (sun_x, sun_y, sun_z))

	glMatrixMode(GL_MODELVIEW)
	glLoadIdentity()
	gluLookAt(distance * math.cos(angle_vertical) * math.sin(angle_horizontal), distance * math.sin(angle_vertical), distance * math.cos(angle_vertical) * math.cos(angle_horizontal), 0, P["PLANET_SCALE"] * look_slope, 0, 0, math.cos(angle_vertical), 0)

	glMatrixMode(GL_PROJECTION)
	glLoadIdentity()
	#glOrtho(-256, 256, -256, 256, 1, 256)
	glFrustum(-2, 2, -2, 2, 3, 512)

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

	#display_cube()
	display_sphere()

	glutSwapBuffers()

def idle(init=False):
	global sun_x, sun_y, sun_z

	now = time.time()
	angle_hour = math.tau * ((now % P["DAY_LENGTH"]) / P["DAY_LENGTH"])
	angle_season = P["TILT"] * math.cos(math.tau * (now % P["YEAR_LENGTH"]) / P["YEAR_LENGTH"])

	sun_distance = 500.0
	sun_x = sun_distance * math.cos(angle_hour) * math.cos(angle_season)
	sun_y = sun_distance * math.sin(angle_season)
	sun_z = sun_distance * math.sin(angle_hour) * math.cos(angle_season)

	if not init:
		display()

parameters_init()

#glutInit(sys.argv)
glutInit()
glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)

imageData, width, height, altitude = sphere.surface(P["IMAGE"], P["STATIC"])

idle(True)

glutInitWindowSize(P["WINDOW_WIDTH"], P["WINDOW_HEIGHT"])
glutInitWindowPosition(256, 256)
window = glutCreateWindow("Planet Generator")
glutDisplayFunc(display)  # Tell OpenGL to call the display method continuously
glutIdleFunc(idle)	 # Draw any graphics or shapes in the display function at all times
glutKeyboardFunc(keyboard)
glutSpecialFunc(keyboard)
glutMouseFunc(mouse)

glViewport(0, 0, P["WINDOW_WIDTH"], P["WINDOW_HEIGHT"])
glClearColor(0, 0, 0, 1)

#glEnable(GL_BLEND)
#glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

texture = glGenTextures(1)
glBindTexture(GL_TEXTURE_2D, texture)
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData)
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)

buffer_vertices, buffer_textures, vertices_count = globe_init(altitude)

glutMainLoop()
