#!/usr/bin/python3

import numpy as np
import math
from PIL import Image

from parameters import P

#the parameters
deep_sea = max(P["SEA_LEVEL"]-.3,0) #for better accuracy
ice_transition_level = P["ICE_LEVEL"]-.05
desert_transition_level = P["DESERT_LEVEL"]+.05 #the bigger the more of a transition
seed = np.random.permutation(256) #permutations, cf Ken Perlin's paper
p = np.concatenate((seed, seed), axis=None)
#the colors
ocean_color = [(15,20,75, 255),(40,70,125, 255)] #from deep to shallow
desert_color = [(202, 204, 75, 255), (241, 204, 155, 255)] #from desert to very desert
forest_color = [(112, 126, 62, 255), (47, 70, 2, 255)] #from forest to very forest
ice_color = [(220, 220, 220, 255), (255, 255, 255, 255)] #from ice to very ice

###########
###tools###

#loading bar cause it takes time
def update_progress(progress, total, message):
    progress_format = int(100*progress*(1/total))+1
    indent = progress_format//5
    print("\r"+message+"[{0}{1}] {2}%".format('#'*indent, ' '*(20-indent), progress_format), end='')

#to map the sphere to 0-1 coords
def sphere_map(x,y):
    longitude = (x*360 - 180) * math.pi /180
    latitude  = (y*180 -  90) * math.pi /180
    return [(math.cos(latitude) * math.cos(longitude) +1) / 2,
            (math.sin(latitude) +1) /2,
            (math.cos(latitude) * math.sin(longitude) + 1) / 2]

#1D map of ensemble [a..b] to [c..d]
def simple_map(x,a,b,c,d):
    return c + ((d-c)/(b-a))*(x-a)

####################################
###colors to make the noise fancy###

#color mapper 
def color_mapper(n, n_min, n_max, color_min, color_max):
    return (simple_map(n, n_min, n_max, color_min[0], color_max[0]),
            simple_map(n, n_min, n_max, color_min[1], color_max[1]),
            simple_map(n, n_min, n_max, color_min[2], color_max[2]),
            255)

#gives color to the noise, use only one parameter for grey scale
def color(*args):
    n = args[0]
    if len(args)==1: return (n*255, n*255, n*255, 255)
    ice = args[1]
    desert = args[2]
    c = None
    terrain = (n+desert+ice)/3 # desert = very 0. ice = very 1
    if n<deep_sea:
        return (15,20,75, 255)
    if n<P["SEA_LEVEL"]:
        return color_mapper(n, deep_sea, P["SEA_LEVEL"], ocean_color[0], ocean_color[1])
    if terrain >P["ICE_LEVEL"]:
        c = color_mapper(terrain, P["ICE_LEVEL"], 1, ice_color[0], ice_color[1])
    elif terrain > ice_transition_level:
        c = color_mapper(terrain, ice_transition_level, P["ICE_LEVEL"], forest_color[1], ice_color[0])
    elif terrain < P["DESERT_LEVEL"]:
        c = color_mapper(terrain, 0, P["DESERT_LEVEL"], desert_color[0], desert_color[1])
    elif terrain < desert_transition_level:
        c = color_mapper(terrain, P["DESERT_LEVEL"], desert_transition_level, desert_color[1], forest_color[0])
    else:
        c = color_mapper(terrain, desert_transition_level, ice_transition_level, forest_color[0], forest_color[1])
    return c

#############################
###sub functions for noise###

#interpolate
def interpolation(a, b, value):
    #i = (1 - math.cos(value * math.pi)) * .5
    #return a*(1-i) + b*i #cosine interpolation
    return ((1 - value) * a + value * b) #linear interpolation

#smoothing
def fade(x):
    return 6*pow(x,5)-15*pow(x,4)+10*pow(x,3)

#dot product with the vectors
def grad(hash, x, y, z):
    h = hash & 15
    u = x if h<8 else y
    v = y if h<4 else (x if h==12 or h==14 else z)
    return (u if (h&1) == 0 else -u) + (v if (h&2==0) else -v)

####################
### Perlin noises###

#generate perlin noise
def noise(x, y, z):
    xi = int(x) % 255
    yi = int(y) % 255
    zi = int(z) % 255
    #print(xi,yi)

    #the distances
    xf = x - int(x)
    yf = y - int(y)
    zf = z - int(z)
    
    #seeds the corners gradients top then bottom
    s0 = p[xi  ]+yi
    s1 = p[xi+1]+yi

    #the distances at each corner
    c0 = grad( p[p[s0]  +zi  ], xf  , yf  , zf   )
    c1 = grad( p[p[s1]  +zi  ], xf-1, yf  , zf   ) 
    c2 = grad( p[p[s0+1]+zi  ], xf  , yf-1, zf   ) 
    c3 = grad( p[p[s1+1]+zi  ], xf-1, yf-1, zf   ) 
    c4 = grad( p[p[s0]  +zi+1], xf  , yf  , zf-1 )
    c5 = grad( p[p[s1]  +zi+1], xf-1, yf  , zf-1 )
    c6 = grad( p[p[s0+1]+zi+1], xf  , yf-1, zf-1 )
    c7 = grad( p[p[s1+1]+zi+1], xf-1, yf-1, zf-1 )

    xf_faded = fade(xf)
    yf_faded = fade(yf)
    zf_faded = fade(zf)


    #bilinear interpolation
    ix0 = interpolation(c0, c1, xf_faded)
    ix1 = interpolation(c2, c3, xf_faded)
    ix2 = interpolation(c4, c5, xf_faded)
    ix3 = interpolation(c6, c7, xf_faded)
    iy0 = interpolation(ix0, ix1, yf_faded)
    iy1 = interpolation(ix2, ix3, yf_faded)
    inter=(interpolation(iy0, iy1, zf_faded)+1)/2 #0-1 noise
    return inter

#add noises with different granularities
def noises(x, y, z):
    total = 0
    norm = sum([pow(P["PERSISTENCE"],i) for i in range(P["OCTAVES"])]) #maps noise back to 0 - 1
    for i in range(P["OCTAVES"]):
        frequency = pow(2,i)
        amplitude = pow(P["PERSISTENCE"],i)
        total = total + noise(x * frequency, y * frequency, z*frequency) * amplitude
    return total / norm

def landmass(w, h):
    data = np.zeros((h, w), dtype=np.float)
    for i in range(h): # lines
        for j in range(w): # columns
            x, y, z = sphere_map(j / (w - 1), i / (h - 1))
            n = noises(x * P["ISLANDS"], y * P["ISLANDS"], z * P["ISLANDS"])
            data[i, j] = n 
        update_progress(i, h, "Landmass noise generation: ")
    print() #prepare for next loading bar
    return data

# gives the texture map as an image
def texture(n, w, h):

    data_ice = np.zeros((h, w), dtype=np.uint8)
    data_desert1 = np.zeros((h, w), dtype=np.uint8)
    data_desert2 = np.zeros((h, w), dtype=np.uint8)
    data_desert3 = np.zeros((h, w), dtype=np.uint8)

    #is there ice?
    def pole_noise(x, y, z, i, j):
        n1 = 1 - fade(abs(math.sin(i*4 / w + noise(x*5,  y*5,  z*5))))
        #n2 = 1 - fade(abs(math.sin(i*4 / w + noise(x*10, y*25, z*25))))
        #return fade(max(n1,n2))
        data_ice[i,j] = n1 * 255
        return n1

    #is it a desert?
    def desert_noise(x, y, z, i, j):
        n = noise(x, y, z)
        m = fade(abs(math.cos(i*4 / w + n))) #the marbelling
        data_desert1[i,j] = (1-fade(n)) * 255
        data_desert2[i,j] = (1-m) * 255
        data_desert3[i,j] = (1-fade((fade(n) + m) / 2)) * 255
        return fade((n + m) / 2)

    data = np.zeros((h, w, 4), dtype=np.uint8)
    for i in range(h): # lines
        for j in range(w): # columns
            x, y, z = sphere_map(j / (w - 1), i / (h - 1))
            ice = pole_noise(x, y, z, i, j)
            desert = desert_noise(x, y, z, i, j)
            data[i, j] = color(n[i, j], ice, desert)
        update_progress(i, h, "Ice and deserts noise generation: ")
    return data, data_ice, data_desert1, data_desert2, data_desert3

def texture_load(landmass, desert, ice, w, h):
    n = landmass

    data = np.zeros((h, w, 4), dtype=np.uint8)
    for i in range(h): # lines
        for j in range(w): # columns
            data[i, j] = color(n[i, j], ice[i // 16, j // 16], desert[i // 16, j // 16])
    return data
